package com.techsparkdemo.utilities

import org.apache.spark.SparkContext
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.sql.types.StructType

/**
  * Created by Stephen Borg on 18/11/2016.
  */
object HiveUtilities {

  @transient private var instance: HiveContext = _

  def get(sparkContext: SparkContext): HiveContext = {
    if (instance == null) {
      instance = new HiveContext(sparkContext)
    }
    instance
  }

  def get(): HiveContext = {
    get(SparkUtilities.get())
  }

  def initialiseHiveContext(): Unit ={
    get().setConf("spark.sql.hive.convertMetastoreOrc", "false")
    get().setConf("spark.sql.shuffle.partitions", "5")
    get().setConf("hive.execution.engine", "tez")
    get().setConf("hive.exec.dynamic.partition", "true")
    get().setConf("hive.exec.dynamic.partition.mode", "nonstrict")
  }

  /**
    * Queries hive table and gets the schema
    */
  def getHiveTableSchema(tableName : String) : StructType = {
    this.get().sql("SELECT * FROM " + tableName + " LIMIT 1 ").schema
  }
}
