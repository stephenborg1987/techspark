package com.techsparkdemo.utilities

import org.apache.spark.SparkContext
import org.apache.spark.streaming.{Duration, StreamingContext}

/**
  * Created by Stephen Borg on 19/11/2016.
  */
object StreamingUtilities {


  @transient private var instance : StreamingContext = _
  @transient private var interval : Duration = _

  def create(sparkContext: SparkContext, duration: Duration) : Unit = {
    if (instance == null) {
      instance = new StreamingContext(sparkContext, duration)
      interval = duration
    }
  }

  def create(duration: Duration) : Unit = {
    create(SparkUtilities.get(), duration)
  }

  def get() : StreamingContext = {
    instance
  }

  def getDuration() : Duration = {
    interval
  }

}

