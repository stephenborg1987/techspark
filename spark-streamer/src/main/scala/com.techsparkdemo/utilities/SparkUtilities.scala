package com.techsparkdemo.utilities

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Stephen Borg on 19/11/2016.
  */
object SparkUtilities {

  @transient private var instance: SparkContext = _

  def create(conf: SparkConf): Unit = {
    if (instance == null) {
      instance = new SparkContext(conf)
    }
  }

  def create(sparkContext: SparkContext): Unit = {
    if (instance == null) {
      instance = sparkContext
    }
  }

  def get(sparkContext: SparkContext): Unit = {
    if (instance == null) {
      instance = sparkContext
    }
    instance
  }

  def get(): SparkContext = {
    instance
  }
}
