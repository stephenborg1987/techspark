package com.techsparkdemo.utilities

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.{Put, Get, HTable, Result}
import org.apache.hadoop.hbase.util.Bytes

/**
  * Created by Stephen Borg on 19/11/2016.
  */
object HBaseUtilities {

  def getHBaseConf(zkQuorumPortless: String,hoyaPath: String): Configuration= {
    val hConf = HBaseConfiguration.create()
    hConf.set("hbase.zookeeper.quorum",zkQuorumPortless )
    hConf.set("zookeeper.znode.parent", hoyaPath )
    hConf
  }

  /**
    * Adds a value to an HBase table
    */
  def addValue(hbaseTable: String,
               rowKey: String,
               columnFamily:String,
               propertyName:String,
               propertyValue: Array[Byte],
               zkQuorumPortless : String,
               hoyaPath : String): Unit ={

    // Get HBase configuration
    val hConf = getHBaseConf(zkQuorumPortless, hoyaPath)

    // Get customers table
    val hTable = new HTable(hConf, hbaseTable)

    val put: Put = new Put(Bytes.toBytes(rowKey))
    put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(propertyName), propertyValue)
    hTable.put(put)
  }

  /**
    * Get's an HBase value
    * */
  def getHBaseValue(hbaseTable: String,rowKey: String,columnFamily:String,propertyName:String,zkQuorumPortless : String, hoyaPath : String) : Result ={

    // Get HBase configuration
    val hConf = getHBaseConf(zkQuorumPortless, hoyaPath)

    // Get customers table
    val hTable = new HTable(hConf, hbaseTable)

    val timestampGet: Get = new Get(Bytes.toBytes(rowKey))
    timestampGet.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(propertyName))

    val result: Result = hTable.get(timestampGet)

    result
  }

  /**
    * Returns a Hbase result based on the hbase table, and also the row key
    */
  def getResultByRowKey(rowKey: String, table: String,zkQuorumPortless : String, hoyaPath : String):Result = {

    // Get HBase configuration
    val hConf = getHBaseConf(zkQuorumPortless, hoyaPath)

    val hTable = new HTable(hConf, table)

    val get = new Get(Bytes.toBytes(rowKey))
    val result = hTable.get(get)
    result

}

}
