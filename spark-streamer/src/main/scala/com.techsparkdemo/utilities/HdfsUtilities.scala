package com.techsparkdemo.utilities

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkContext

/**
  * Created by Stephen Borg on 18/11/2016.
  */
object HdfsUtilities {

  @transient private var instance : FileSystem = _

  def get(sparkContext : SparkContext) : FileSystem = {
    if (instance == null) {
      instance = FileSystem.get(sparkContext.hadoopConfiguration)
    }
    instance
  }

  def get() : FileSystem = {
    get(SparkUtilities.get())
  }

  def cleanSparkTmpDir(tableDir : String,hdfsPath : String) = {
    val hdfs : FileSystem = HdfsUtilities.get()
    val conf : Configuration = hdfs.getConf()
    val path : String = hdfsPath + tableDir + "/.hive-staging_hive*"

    hdfs.globStatus(new Path(path))
      .foreach(x => hdfs.delete(x.getPath, true))
  }
 }
