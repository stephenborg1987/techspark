package com.techsparkdemo.models

import org.apache.spark.sql.Row
import org.json.JSONObject


/**
  * Created by Stephen Borg on 19/11/2016.
  */
case class Purchase(
                     itemPrice :Float,
                     quantity :  Int,
                     currencyCode: String,
                     email :String,
                     ip :String,
                     username: String,
                     customerId: String,
                     countryCode: String,
                     paymentMethodName: String,
                     timestamp :Long,
                     productName :String,
                     productCategory: String,
                     longitude :String,
                     latitude :String,
                     cardNumber :String,
                     isPremium : Boolean,
                     fullJSONObject : String){

  def getPurchaseFromJSON(message: String): Purchase = {

    val jsonObj: JSONObject = new JSONObject(message)

    Purchase(jsonObj.getDouble("itemPrice").toFloat,
      jsonObj.getInt("quantity"),
      jsonObj.getString("currencyCode"),
      jsonObj.getString("email"),
      jsonObj.getString("ip"),
      jsonObj.getString("username"),
      jsonObj.getString("customerId"),
      jsonObj.getString("countryCode"),
      jsonObj.getString("paymentMethodName"),
      jsonObj.getLong("timestamp"),
      jsonObj.getString("productName"),
      jsonObj.getString("productCategory"),
      jsonObj.getString("longtitude"),
      jsonObj.getString("latitude"),
      jsonObj.getString("cardNumber"),
      jsonObj.getBoolean("premium"),
      message)
  }

  def getRowFromPurchase(message: Purchase): Row = {

    Row(itemPrice,
      message.quantity,
      message.currencyCode,
      message.email ,
      message.ip,
      message.username,
      message.customerId,
      message.countryCode,
      message.paymentMethodName,
      message.timestamp,
      message.productName ,
      message.productCategory,
      message.longitude ,
      message.latitude ,
      message.cardNumber,
      message.isPremium,
    message.fullJSONObject)
  }

  def this(){
    this(0,0,null,null,null,null,null,null,null,0,null,null,null,null,null,false,null)
  }
}
