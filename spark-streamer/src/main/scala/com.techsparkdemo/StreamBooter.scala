package com.techsparkdemo

import java.util.Date

import com.techsparkdemo.helpers.Utilities
import com.techsparkdemo.utilities.{HBaseUtilities, SparkUtilities, StreamingUtilities}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.SparkConf
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.kafka.KafkaUtils
import org.joda.time.DateTime

/**
  * Created by Stephen Borg on 16/08/2016.
  */
object StreamBooter {

  def main(args: Array[String]) {

    var checkPointPath =""
    var appName =""
    var masterMode =""
    var batchSeconds = 0
    var zkQuorum =""
    var hdfsPath = ""
    var kafkaStreamingGroup = ""
    var kafkaTopic = ""
    var znodeParentPath = ""
    var numThreads = 0

    if (args.length < 1) {
      println("*********************************")
      println("Taking default values...")
      println("Usage: [sparkCheckpoint] [appName] [masterMode] [batchSeconds] [zkQuorum] [hdfsPath] [kafkaStreamingGroup] [kafkaTopic] [znodeParentPath] [numThreads]")
      println("*********************************")

      // Create checkpoint, so when the streaming stops/starts, it may resume from proper
      // offset
      checkPointPath = "/tmp/SparkCheckpoints/StreamingCheckpoint"
      appName = "Tech Spark Demo"
      masterMode="local[*]"
      batchSeconds=15
      zkQuorum = "104.40.216.218:2181,52.174.110.65:2181,13.95.23.152:2181"
      hdfsPath = "hdfs://services03.stephenborg.com:8020"
      kafkaStreamingGroup = "purchasesStrGroup"
      kafkaTopic = "Purchases"
      znodeParentPath = "/hbase-unsecure"
      numThreads = 10
    }else
    {
      System.out.println("Extracting parameters from arguments...")
      checkPointPath = args(0)
      appName = args(1)
      masterMode =args(2)
      batchSeconds =args(3).toInt
      zkQuorum = args(4)
      hdfsPath = args(5)
      kafkaStreamingGroup = args(6)
      kafkaTopic = args(7)
      znodeParentPath = args(8)
      numThreads = args(9).toInt
    }

    // Configure spark configuration
    val conf = new SparkConf()
      .setAppName(appName)
      .setMaster(masterMode)

    // Create spark context
    SparkUtilities.create(conf)

    // We will stream and process batches of 5 seconds
    StreamingUtilities.create(Seconds(batchSeconds))

    System.out.println("*************************")
    System.out.println("*************************")
    System.out.println("Application will begin shortly...")
    System.out.println("*************************")
    System.out.println("*************************")

    // To confirm configs
    Thread.sleep(10000)

    // We first need to get the hive table schema in order for us to map the stream to
    // val tableSchema = HiveUtilities.getHiveTableSchema(hiveTableName)

    // Map topics
    val topicsSet = Map(kafkaTopic -> numThreads)

    // Set stream
    val kafkaStream = KafkaUtils.createStream(StreamingUtilities.get(), zkQuorum, kafkaStreamingGroup, topicsSet)

    // Map a stream and start getting objects
    val messages = kafkaStream.map((data: (String, String)) => Utilities.getObjects(data._2))

    // Ignore those have NULL as customer Id
    messages.foreachRDD(rdd => {

      rdd.foreach(x=> {

        var score = 0;
        val prefixMessage = "Customer : " + x.customerId + " "
        var finalMessage = ""

        // 1. Check if stolen cards
        val stolenCardCheck = HBaseUtilities.getResultByRowKey(x.cardNumber, "StolenCards", zkQuorum.replace(":2181", ""), znodeParentPath)

        if (!stolenCardCheck.isEmpty) {
          finalMessage = finalMessage +  " Stolen card with number : " + x.cardNumber + "\n"
          score=score+100;
        }

        // 2. Check if it's expensive, and this is a premium product!
        if (x.isPremium) {
          finalMessage = finalMessage + " Bought a premium product : " + x.productName + "\n"
          score=score+10;
        }

        // 3. Check if the quantity is larger than usual
        val quantityCheck = HBaseUtilities.getHBaseValue("AverageQuantities", x.productName, "data", "averageQuantity", zkQuorum.replace(":2181", ""), znodeParentPath)
        if (quantityCheck.value() != null) {
          if (x.quantity > Bytes.toString(quantityCheck.value).toInt) {
            finalMessage = finalMessage + " Brought a larger than usual quantity. Avg quantity : " + Bytes.toString(quantityCheck.value).toInt + ", customer brought : " + x.quantity + "\n"
            score=score+10;
          }
        }

        // 4. Check if the customer in question has just purchased another item a while ago. This needs to be based on
        //    Credit card and not customer, as can have multiple accounts for fraud
        val timestamp = HBaseUtilities.getHBaseValue("CustomerInfo", x.customerId, "info", "LastPurchaseDate", zkQuorum.replace(":2181", ""), znodeParentPath)
        if (timestamp.value() != null) {

          // Now time
          val unixTime: Long = System.currentTimeMillis / 1000L

          val lastTransactionTime: Long = Bytes.toLong(timestamp.value)

          // Usually 1 TX per customer every 10 minutes
          if (unixTime - lastTransactionTime > (60 * 10)) {
            finalMessage = finalMessage + " Buying with an avg rate higher than usual." + "\n"
          }
          score=score+5;
        }

        // 5. Check country of the customer and see if this is odd hours!
        val countryPeakStart = HBaseUtilities.getHBaseValue("CountryPurchaseUsualHours", x.countryCode, "info", "peakHourStart", zkQuorum.replace(":2181", ""), znodeParentPath)
        val countryPeakEnd = HBaseUtilities.getHBaseValue("CountryPurchaseUsualHours", x.countryCode, "info", "peakHourEnd", zkQuorum.replace(":2181", ""), znodeParentPath)
        if (countryPeakStart.value() != null && countryPeakEnd.value() != null) {

          val nowHour = new Date().getHours()

          // Usually 1 TX per customer every 10 minutes
          if (nowHour > Bytes.toString(countryPeakStart.value).toInt &&
            nowHour < Bytes.toString(countryPeakEnd.value).toInt) {
            finalMessage = finalMessage + " Is from " + x.countryCode + " with an odd purchase time." + "\n"
          }
          score=score+5;
        }

        // 6. Check if risky country
        val isRiskyCountry = HBaseUtilities.getHBaseValue("RiskyCountries", x.countryCode, "info", "isRisky", zkQuorum.replace(":2181", ""), znodeParentPath)
        if (isRiskyCountry.value() != null) {

          // Usually 1 TX per customer every 10 minutes
          if (Bytes.toString(isRiskyCountry.value())=="true") {
            finalMessage = finalMessage + " Is from a risky country. Country : " + x.countryCode + "\n"
          }
          score=score+10;
        }

        // Register this purchase so next time round we can identity that the customer has placed an order before
        // and we can compare the time difference
        HBaseUtilities.addValue("CustomerInfo", x.customerId, "info", "LastPurchaseDate", Bytes.toBytes(x.timestamp), zkQuorum.replace(":2181", ""), znodeParentPath)

        // Compare all results
        if (score > 30)  {
            HBaseUtilities.addValue("PotentialFraudStream", x.customerId, "info", "Country", Bytes.toBytes(x.countryCode), zkQuorum.replace(":2181", ""), znodeParentPath)
            HBaseUtilities.addValue("PotentialFraudStream", x.customerId, "info", "Day", Bytes.toBytes(new DateTime(x.timestamp).toString("yyyy-MM-dd")), zkQuorum.replace(":2181", ""), znodeParentPath)
            HBaseUtilities.addValue("PotentialFraudStream", x.customerId, "info", "Score", Bytes.toBytes(score), zkQuorum.replace(":2181", ""), znodeParentPath)
            HBaseUtilities.addValue("PotentialFraudStream", x.customerId, "info", "Lon", Bytes.toBytes(x.longitude), zkQuorum.replace(":2181", ""), znodeParentPath)
            HBaseUtilities.addValue("PotentialFraudStream", x.customerId, "info", "Lat", Bytes.toBytes(x.latitude), zkQuorum.replace(":2181", ""), znodeParentPath)
            HBaseUtilities.addValue("PotentialFraudStream", x.customerId, "info", "Message", Bytes.toBytes(prefixMessage + finalMessage), zkQuorum.replace(":2181", ""), znodeParentPath)
            HBaseUtilities.addValue("PotentialFraudStream", x.customerId, "info", "LastTransactionTime", Bytes.toBytes(x.timestamp), zkQuorum.replace(":2181", ""), znodeParentPath)
            HBaseUtilities.addValue("PotentialFraudStream", x.customerId, "info", "LastTransactionJSON", Bytes.toBytes(x.fullJSONObject), zkQuorum.replace(":2181", ""), znodeParentPath)
          }
        })
      })

      messages.print
      StreamingUtilities.get().start
      StreamingUtilities.get().awaitTermination
  }
}
