package com.techsparkdemo.helpers

import com.techsparkdemo.models.Purchase
import com.techsparkdemo.utilities.{HdfsUtilities, HiveUtilities}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{SaveMode, Row}
import org.apache.spark.sql.types.StructType
import org.codehaus.jettison.json.JSONObject

/**
  * Created by Stephen Borg on 19/11/2016.
  */
object Utilities {
  /*
   * Get required properties from a Payment TX
   * */
  def getObjects(message: String): Purchase = {

    val jsonObj: JSONObject = new JSONObject(message)
    val gt = new Purchase()
    gt.getPurchaseFromJSON(message)
  }

  /**
    * Streaming TX in realtime, and writing to Apache Hive directly using the Apache Spark hive streaming to ORC as a table
    * format.
    * */
  def writeToHive(rdd:RDD[Purchase] ,
                      partitioningField: String,
                      realtimeStreamingPath : String,
                      tableStructure : StructType,
                      tableName : String,
                      hdfsPath : String): Unit ={

    System.out.println("RDD processing!")

    // Convert records from TransactionWrapper traits, to actual Row
    val newRdd = rdd.map(x=>{Row(x.getRowFromPurchase(x))})

    System.out.println("RDD Count : " + rdd.count())

    // The loading of the schema needs to be
    // Convert into a DF and save
    HiveUtilities.get(newRdd.sparkContext)
      .createDataFrame(newRdd, tableStructure)
      .write
      .format("orc")
      .partitionBy(partitioningField)
      .options(Map("path" -> realtimeStreamingPath))
      .mode(SaveMode.Append)
      .insertInto(tableName)

    System.out.println("Clean!")

    // Remove temporary files!
    HdfsUtilities.cleanSparkTmpDir(realtimeStreamingPath,hdfsPath)

  }
}
