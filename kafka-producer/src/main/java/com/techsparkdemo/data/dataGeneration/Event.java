package com.techsparkdemo.data.dataGeneration;

import com.techsparkdemo.data.entities.Purchase;
import com.techsparkdemo.data.producers.KafkaProducer;
import org.fluttercode.datafactory.impl.DataFactory;

import javax.xml.crypto.KeySelector;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Stephen Borg on 18/11/2016.
 */

public class Event extends TimerTask
{
    private static int totalNumberOfEventsPerRun = 500;

    public void run()
    {
        // create a pool of threads, 10 max jobs will execute in parallel
        ExecutorService threadPool = Executors.newFixedThreadPool(totalNumberOfEventsPerRun);

        KafkaProducer producer = new KafkaProducer();
        producer.initialize();

        // submit jobs to be executing by the pool
        for (int i = 0; i < totalNumberOfEventsPerRun; i++) {
            threadPool.submit(new Runnable() {

                public void run() {
                    try
                    {
                        producer.publishMesssage(generateEvent());
                    } catch (Exception e)
                    {
                        // For demo purposes, we are not going to log errors/send to a kafka stream
                        e.printStackTrace();
                    }
                }
            });
        }

        try
        {
            // once you've submitted your last job to the service it should be shut down
            threadPool.shutdown();
            // wait for the threads to finish if necessary
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e)
        {
            // For demo purposes, we are not going to log errors/send to a kafka stream
            e.printStackTrace();
        }

        producer.close();
    }

    /**
     * Mock a transaction for testing purposes
     * */
    private Purchase generateEvent(){
        try
        {
            DataFactory defaultFactory = new DataFactory();

            // Test data  generated from a few websites online...
            float itemPrice = defaultFactory.getNumberBetween(0, Integer.MAX_VALUE);
            int quantity = defaultFactory.getNumberBetween(0, Integer.MAX_VALUE);
            String baseCurrencyCode = defaultFactory.getItem(StaticTestData.currencies, 80, "EUR");
            String email = defaultFactory.getItem(StaticTestData.emails, 50, "stephenborg@gmail.com");
            String customerId = UUID.randomUUID().toString();
            String countryCode = defaultFactory.getItem(StaticTestData.countryCodes, 100, "MT");
            String paymentMethodName = defaultFactory.getItem(StaticTestData.paymentMethods, 100, "Neteller");
            String longLats = defaultFactory.getItem(StaticTestData.longLat, 100, "4.62283, 39.61817");
            Date timestamp = new Date();
            String productName = defaultFactory.getItem(StaticTestData.productNames, 100, "AK-47");
            String ipAddress = defaultFactory.getItem(StaticTestData.ips, 100, "125.158.191.198");
            String productCategory = defaultFactory.getItem(StaticTestData.productCategories, 100, "Misc.");
            String card = defaultFactory.getItem(StaticTestData.cards, 50, "3584237251420382");
            String longitude = longLats.split(",")[0];
            String latitude=longLats.split(",")[1].trim();
            boolean isPremium = Math.random() < 0.5;

            return new Purchase(itemPrice,quantity,baseCurrencyCode,email,ipAddress,email,customerId,countryCode,paymentMethodName,timestamp,productName,productCategory,longitude,latitude,card,isPremium);


        } catch (Exception e)
        {

            // For demo purposes, we are not going to log errors/send to a kafka stream

            throw e;
        }
    }
}
