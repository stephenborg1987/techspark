package com.techsparkdemo.data.dataGeneration;

import org.fluttercode.datafactory.NameDataValues;
import org.fluttercode.datafactory.impl.DefaultNameDataValues;

/**
 * Created by Stephen Borg on 18/11/2016.
 */
public class BrandGenerator  implements NameDataValues
{
    String[] firstNames = {"Orange Brand","Green Brand","Purple Brand","Yellow Brand"};

    NameDataValues defaults = new DefaultNameDataValues();

    public String[] getFirstNames() {
        return firstNames;
    }

    public String[] getLastNames() {
        return defaults.getLastNames();
    }

    public String[] getPrefixes() {
        return defaults.getPrefixes();
    }

    public String[] getSuffixes() {
        return defaults.getSuffixes();
    }

}
