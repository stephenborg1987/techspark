package com.techsparkdemo.data.producers;


import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.Properties;
/**
 * Created by Stephen Borg on 14/11/2016.
 */
/**
 *
 */
public class KafkaProducer
{
    private static Producer<Integer, String> producer;
    private static final String topic= "Purchases";

    public void initialize() {
        Properties producerProps = new Properties();
        producerProps.put("metadata.broker.list", "services01.stephenborg.com:6667," +
                "services02.stephenborg.com:6667," +
                "services03.stephenborg.com:6667");

        producerProps.put("serializer.class", "kafka.serializer.StringEncoder");
        producerProps.put("request.required.acks", "1");
        ProducerConfig producerConfig = new ProducerConfig(producerProps);
        producer = new Producer<Integer, String>(producerConfig);
    }

    public void publishMesssage(Object content) throws Exception{

        ObjectMapper mapper = new ObjectMapper();
        //Object to JSON in String
        String jsonInString = mapper.writeValueAsString(content);

        //Define topic name and message
        KeyedMessage<Integer, String> keyedMsg = new KeyedMessage<Integer, String>(topic,(jsonInString));

        // Attempt to send message
        producer.send(keyedMsg); // This publishes message on given topic

        // Notify
        System.out.println("Message fired : " + content);

    }

    public void close(){

        // Close
        producer.close();
    }

}
