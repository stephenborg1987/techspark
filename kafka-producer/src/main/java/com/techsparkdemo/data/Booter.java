package com.techsparkdemo.data;


import java.util.Timer;

/**
 * Created by Stephen Borg on 18/11/2016.
 */
public class Booter
{
    public static void main(String[] args) throws Exception {
        Timer timer = new Timer();
        timer.schedule(new com.techsparkdemo.data.dataGeneration.Event(), 0, 1);
    }
}

