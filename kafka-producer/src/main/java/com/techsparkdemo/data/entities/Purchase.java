package com.techsparkdemo.data.entities;

import javax.swing.text.StyledEditorKit;
import java.util.Date;

/**
 * Created by Stephen Borg on 14/11/2016.
 */
public class Purchase
{
    private float itemPrice;
    private int quantity;
    private String currencyCode;
    private String email;
    private String ip;
    private String username;
    private String customerId;
    private String countryCode;
    private String paymentMethodName;
    private Date timestamp;
    private String productName;
    private String productCategory;
    private String longtitude;
    private String latitude;
    private String cardNumber;
    private boolean isPremium;

    public Purchase(float itemPrice, int quantity, String currencyCode, String email, String ip, String username, String customerId, String countryCode, String paymentMethodName, Date timestamp, String productName, String productCategory,String longtitude,String latitude,String card,boolean isPremium)
    {
        this.itemPrice = itemPrice;
        this.quantity = quantity;
        this.currencyCode = currencyCode;
        this.email = email;
        this.ip = ip;
        this.username = username;
        this.customerId = customerId;
        this.countryCode = countryCode;
        this.paymentMethodName = paymentMethodName;
        this.timestamp = timestamp;
        this.productName = productName;
        this.productCategory = productCategory;
        this.longtitude = longtitude;
        this.latitude = latitude;
        this.cardNumber = card;
        this.isPremium = isPremium;
    }


    public boolean isPremium()
    {
        return isPremium;
    }

    public void setPremium(boolean premium)
    {
        isPremium = premium;
    }

    public String getCardNumber()
    {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber)
    {
        this.cardNumber = cardNumber;
    }

    public String getLongtitude()
    {
        return longtitude;
    }

    public void setLongtitude(String longtitude)
    {
        this.longtitude = longtitude;
    }

    public String getLatitude()
    {
        return latitude;
    }

    public void setLatitude(String latitude)
    {
        this.latitude = latitude;
    }

    public float getItemPrice()
    {
        return itemPrice;
    }

    public void setItemPrice(float itemPrice)
    {
        this.itemPrice = itemPrice;
    }

    public int getQuantity()
    {
        return quantity;
    }

    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

    public String getCurrencyCode()
    {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode)
    {
        this.currencyCode = currencyCode;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getCustomerId()
    {
        return customerId;
    }

    public void setCustomerId(String customerId)
    {
        this.customerId = customerId;
    }

    public String getCountryCode()
    {
        return countryCode;
    }

    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getPaymentMethodName()
    {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName)
    {
        this.paymentMethodName = paymentMethodName;
    }

    public Date getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp(Date timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public String getProductCategory()
    {
        return productCategory;
    }

    public void setProductCategory(String productCategory)
    {
        this.productCategory = productCategory;
    }
}

